import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
import matplotlib.animation as animation 
from datetime import datetime
import copy
from matplotlib import colors
import gc
#import time

'''
Todo/WiP: Polygonpunkte hinzufügen und über diese animieren

Punkte speichern Rotation und Koordinaten im C2, mgl Zoom?

dann jewils Punkte 1->2->...n->1 animieren

animieren von a nach b:
Koordinaten von a zu b laufen lassen und alpha_a zu alpha_b  drehen

Koord durch Bezier mit 4 punkten, a ,a + alpha_a, b - alpha_b, b sodass alpha_a/b als Vektoren im C2 betrachtet werden
'''

def Casteljau(b: np.ndarray, t: float):
    '''
    Bezier Curve using Casteljau
        Parameters:
            b(numpy array): Controlpoints as columns
            t(float): at which point the Bezier Curve should be calculated
        Returns:
            vector: Point at which the Bezier Curve made by b ist in t
    '''
    n = b.shape[0]
    B = np.zeros((n,n,b.shape[1]), dtype = b.dtype)
    for i in range(n):
        B[i,n-1,:] = b[i,:]
    for j in range(n-1):
        l = n-2-j
        for i in range(l+1):
            B[i,l,:] = ((1-t)*B[i,l+1,:]+t*B[i+1,l+1,:])
    return(B[0,0])

def create_Frame_Images(Points: list, Iterations: int, Resolution: int, Frames_amount: int, Soft: bool):
    '''
    Frames between each Pointpair
    Points are formed: Coords, Alpha, Span, Center
    '''
    def add_spline_points(A: list):
        '''
        add points to interpolate the Cubic Bezier Curves
        Points are added in the general Direction of surrounding ones
        '''
        B = []
        n = len(A)
        B.append(A[0])
        B.append(A[0]+(A[1]-A[n-1])/2)
        for i in range(1,n-1):
            B.append(A[i]-(A[i+1]-A[i-1])/2)
            B.append(A[i])
            B.append(A[i]+(A[i+1]-A[i-1])/2)
        B.append(A[n-1]-(A[0]-A[n-2])/2)
        B.append(A[n-1])
        return B

    def get_Frame_Points(in_list_: list, power: int):
        '''
        add the points in between frames on the Bezier Curve
        creating a smoother animation
        '''
        in_list = add_spline_points(in_list_)
        frame_points_ = []
        for i in range(0, len(in_list)-1, power):
            temp_array = np.array([[in_list[i]],[in_list[i+1]],[in_list[i+2]],[in_list[i+3]]])
            for j in range(Frames_amount):
                frame_points_.append(Casteljau(temp_array, j/Frames_amount))
        return frame_points_

    Points.append(Points[0])
    # add the first Point to the end so that the animation is continuous on repeat
    Points_a = np.array(Points, dtype = 'object')
    Frames = []

    point = get_Frame_Points(Points_a[:,0], 3)
    alpha = get_Frame_Points(Points_a[:,1], 3)
    Span = get_Frame_Points(Points_a[:,2], 3)
    Center = get_Frame_Points(Points_a[:,3], 3)
    # get all of the values for every Frame

    for k in range(len(point)-1):
        #non linear/ Bezier Curves hitting the specified points
        Temp_Span = Span[k]
        Temp_Center = [Center[k].real, Center[k].imag]

        Temp_Iterations = int(6*Iterations/Temp_Span)
        Temp_edge = [Temp_Center[0]-Temp_Span/2, Temp_Center[0]+Temp_Span/2, Temp_Center[1]-Temp_Span/2, Temp_Center[1]+Temp_Span/2]

        x_cor = np.linspace(Temp_edge[0], Temp_edge[1], Resolution)
        y_cor = np.linspace(Temp_edge[2], Temp_edge[3], Resolution)

        Frames.append(m_j_section(Temp_Iterations, point[k], alpha[k], x_cor, y_cor, Resolution, True, True))
            
        print(format((k)/len(point)*100, ".2f"), "%, (", k, "out of ", len(point), " Frames have been created.)")

    return Frames, [point, alpha, Center]


def m_j_section(ITERATIONS: int, Point: complex ,alpha: float, x_cor: np.ndarray, y_cor: np.ndarray, SIZE: int, Mandel: bool, Julia: bool):
    '''
    calculates a plane section of the mandeljulia at the 'Point' intersecting the mandelbrot set with rotation 'alpha'

    returns: 
        np_Array with values that show the divergence of points
    '''

    theta = np.radians(alpha) # convert angle to radians
    co, si = np.cos(theta), np.sin(theta) # calculate cos and sin -> the length in the unitcircle

    cor = np.zeros((SIZE,SIZE), dtype = complex) # cor is the coordinate array containing each pixel
    cor.real, cor.imag = np.meshgrid(x_cor, y_cor)

    output = np.zeros((SIZE,SIZE),dtype=np.uint8)
    elements_todo = np.ones((SIZE, SIZE), dtype=bool) # a bool-mask 

    Off = np.zeros(2, dtype=complex)

    if Mandel:
        Off[0] = (Point * co) # if the Mandel plane should influence the plane
    if Julia:
        Off[1] = (Point * si) # if the Julia plane should influence the plane

    z_ = cor * si + Off[0]
    c_ = cor * co + Off[1]

    for iteration in range(1, ITERATIONS + 1):
        z_[elements_todo] = (z_[elements_todo]**2 + c_[elements_todo])
        mask = np.logical_and((z_.real**2 + z_.imag**2) > 4, elements_todo)
        elements_todo = np.logical_and(elements_todo, np.logical_not(mask))
        output[mask] = iteration
        
    gc.collect()
    return output

def draw_divergence(Point: complex, Julia_Point: complex, Iter: int, alpha: float, Mandel: bool, Julia: bool, calc: list):
    '''
    draws the divergence of a Point given the current intersection plane
    calc:=  [x_lim, y_lim, span, edge]
    '''
    theta = np.radians(alpha)
    co, si = np.cos(theta), np.sin(theta)

    Off = np.zeros(2, dtype=complex)

    if Mandel:
        Off[0] = (Julia_Point * co)
    if Julia:
        Off[1] = (Julia_Point * si)

    z = Point * si + Off[0]
    c = Point * co + Off[1]
    
    div_Points = [[(Point.real - calc[3][0])/calc[2] * calc[0][1],(Point.imag - calc[3][2])/calc[2] * calc[1][1]]]

    for iteration in range(Iter):
        z = (z**2 + c)
        div_Points.append([(z.real - calc[3][0])/calc[2] * calc[0][1], (z.imag - calc[3][2])/calc[2] * calc[1][1]])
        if (z.real**2 + z.imag**2 > 25):
            break

    div = np.array(div_Points)
    div_Points_sct.set_offsets(div_Points)
    div_Points_plt.set_data([div[:,0],div[:,1]])
    fig.canvas.draw()

def reset():
    '''
    resets to original Size, Iteration and Zoom
    '''
    global edge, span, x_cor, y_cor, ITERATIONS, SIZE, Point
    SIZE = 512
    ITERATIONS = 50
    edge = [-3,3,-3,3]
    span = max(edge[1] - edge[0], edge[3] - edge[2])
    x_cor = np.linspace(edge[0], edge[1], SIZE)
    y_cor = np.linspace(edge[2], edge[3], SIZE)

def button_press(event):
    '''
    Leftclick
        shows the divergence of the on-clicked Point
    '''
    if event.inaxes != ax1:
        return
    if event.xdata == None or event.ydata == None:
        return
    if event.button == 1:
        global Pressed
        Pressed = True

def motion_notify_callback(event):
    '''
    if left mouse-button is held:
        shows the divergence of the on-clicked Point
    '''
    if event.inaxes != ax1:
        return
    if event.xdata == None or event.ydata == None:
        return
    if event.button == 1 and Pressed and Draw_Div_Points:
        x_lim = ax1.get_xlim()
        y_lim = ax1.get_ylim()
        div_Point = complex(event.xdata/x_lim[1]*span+edge[0], event.ydata/y_lim[1]*span+edge[2])
        draw_divergence(div_Point, Point, int(ITERATIONS/2), alpha.val, Mandel, Julia, [x_lim, y_lim, span, edge])

def button_release(event):
    '''
    Rightclick release:
        sets the Julia location to the point of the click
    Leftclick release:
        shows the divergence of the on-clicked Point
    '''
    if event.inaxes != ax1:
        return
    if event.xdata == None or event.ydata == None:
        return
    x_lim = ax1.get_xlim()
    y_lim = ax1.get_ylim()
    global Point, div_Point, ITERATIONS
    if event.button == 3:
        Point = complex(event.xdata/x_lim[1]*span+edge[0], event.ydata/y_lim[1]*span+edge[2])
        update(1)
    elif event.button == 2:
        return
    elif event.button == 1:
        global Pressed
        div_Point = complex(event.xdata/x_lim[1]*span+edge[0], event.ydata/y_lim[1]*span+edge[2])
        Pressed = False
        if not Draw_Div_Points:
            return
    draw_divergence(div_Point, Point, 2*ITERATIONS, alpha.val, Mandel, Julia, [x_lim, y_lim, span, edge])



def update(val):
    '''
    updates the figure and redraws the mandeljulia, this gets called after every settingschange
    '''
    draw_fractal = m_j_section(ITERATIONS, Point, alpha.val, x_cor, y_cor, SIZE, Mandel, Julia)

    ax1.pcolormesh(draw_fractal, shading = 'flat', cmap='twilight_shifted', zorder = 1)
    ax1.set_title("Julia over "+str(Point)+r", $\alpha$ = " + str(alpha.val) + " Iterations = " + str(ITERATIONS))

    if Draw_Div_Points:
        x_lim = ax1.get_xlim()
        y_lim = ax1.get_ylim()
        draw_divergence(div_Point, Point, 2*ITERATIONS, alpha.val, Mandel, Julia, [x_lim, y_lim, span, edge])
    else:
        fig.canvas.blit()
    
    del draw_fractal
    gc.collect()

def key_press(event):
    '''
    gets called when keys are pressed

    Keyfunctions:
        p: prints the Point over which Julia ist defined, the current edges and the frame_points in order of input

        P: enables/disables showing the divergence of mouse-specific points

        r: resets to original values (see reset)

        +/-: zooms in/out of the current figure

        i/I: increases/lowers the Iterations

        m/j: sets alpha to 0/90 to show mandelbrot or current julia set

        M/J: change wether the Point influences the creation of Mandel/Julia-Set

        a: add a frame_point for the animation

        A: create the animation

        d/D: delete the last/all frame_points

        Arrow_keys: move in the plane orthogonal to this one
    '''
    global span, edge, x_cor, y_cor, ITERATIONS, Point, Mandel, Julia, Draw_Div_Points

    x_lim = ax1.get_xlim()
    y_lim = ax1.get_ylim()

    offset = 0.05*span/6
    # the amount that the arrow keys move the plane

    if not event.inaxes:
        return

    if event.key == 'p':
        print("Fractal over: ", Point, "current edges: ", edge, "\nThese are the current frame_points")
        print("".join(["Point: "+str(item[0])+", Alpha: "+str(item[1])+", Span: "+str(item[2])+", Centerpoint: "+str(item[3])+"\n" for item in Frame_Points]))
        return

    elif event.key == 'P':
        if Draw_Div_Points:
            div_Points_sct.set_alpha(0)
            div_Points_plt.set_alpha(0)
        else:
            div_Points_sct.set_alpha(1)
            div_Points_plt.set_alpha(0.25)
        Draw_Div_Points = not Draw_Div_Points
        fig.canvas.draw()
        return

    elif event.key == 'r':
        reset()

    elif event.key == '+' or event.key == '-':
        if event.xdata == None or event.ydata == None:
            return

        mid_x = event.xdata/x_lim[1]*span+edge[0]
        mid_y = event.ydata/y_lim[1]*span+edge[2]

        if event.key == '+':
            ITERATIONS = int(1.15*ITERATIONS)
            span = span/2
        else:
            ITERATIONS = int(ITERATIONS/1.15)
            span = span*2

        edge = [mid_x-span/2, mid_x+span/2, mid_y-span/2, mid_y+span/2]
        x_cor = np.linspace(edge[0], edge[1], SIZE)
        y_cor = np.linspace(edge[2], edge[3], SIZE)

    elif event.key == 'x':
        alpha.set_val(alpha.val + 15)
    elif event.key == 'X':
        alpha.set_val(alpha.val - 15)

    elif event.key == 'i':
        ITERATIONS = int(ITERATIONS*1.15)

    elif event.key == 'I':
        ITERATIONS = int(ITERATIONS/1.15)

    elif event.key == 'm':
        alpha.set_val(0)
    
    elif event.key == 'M':
        Mandel = not Mandel

    elif event.key == 'j':
        alpha.set_val(90)

    elif event.key == 'J':
        Julia = not Julia

    elif event.key == 'up':
        Point = complex(Point.real, Point.imag + offset)
    elif event.key == 'down':
        Point = complex(Point.real, Point.imag - offset)
    elif event.key == 'right':
        Point = complex(Point.real + offset, Point.imag)
    elif event.key == 'left':
        Point = complex(Point.real - offset, Point.imag)

    elif event.key == 'a':
        #event.xdata/x_lim[1]*span+
        mid_x = (edge[0]+edge[1])/2
        mid_y = (edge[2]+edge[3])/2
        Frame_Points.append([copy.deepcopy(Point), copy.deepcopy(alpha.val), copy.deepcopy(span), copy.deepcopy(complex(mid_x, mid_y))])
        return
    elif event.key == 'd':
        Frame_Points.pop()
        return
    elif event.key == 'D':
        Frame_Points.clear()
        return
    
    elif event.key == 'A':
        if len(Frame_Points) < 2:
            print("You have too few Points for an animation")
            return

        plt.close() # close the plot
        plt.clf() # clear figure
        plt.cla() # and axis object

        fig2 = plt.figure(figsize=(11, 11))  # instantiate a figure to draw
        ax2 = plt.axes()  # create an axes object

        ax2.get_xaxis().set_visible(False)
        ax2.get_yaxis().set_visible(False)

        print("Frames will now render")

        Frames, [point, Alpha, Center] = create_Frame_Images(Frame_Points, 150, 2000, 25, False)

        print("Animation will now render")

        light = colors.LightSource(azdeg=315, altdeg=5)

        def animate(i):
            ax2.clear()  # clear axes object
            M = light.shade(Frames[i], cmap=plt.cm.gist_earth, vert_exag=1.5, norm=colors.PowerNorm(0.1), blend_mode='hsv')
            img = ax2.imshow(M, extent=[x_lim[0], x_lim[1], y_lim[0], y_lim[1]], interpolation="bicubic")
            ax2.set_title("P: "+str(format(point[i][0].real, ".3f"))+" "+str(format(point[i][0].imag, ".3f"))+"i "+r", $\alpha$ = "+str(format(Alpha[i][0],".2f"))+" C: "+str(format(Center[i][0].real, ".3f"))+" "+str(format(Center[i][0].imag, ".3f"))+"i")
            return [img]

        ani = animation.FuncAnimation(fig2, animate, frames=len(Frames), interval=120, blit = True, cache_frame_data = False)
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=30, metadata={'artist': 'Jan Förster'}, bitrate=-1)

        dt_string = datetime.now().strftime("%Y_%m_%d_%H_%M")

        ani.save("Output_Archive/MandelJulia/MandelJulia_Animation_" + dt_string + ".mp4", writer = writer)
        print("The Animation has rendered")
        return
    else:
        return

    update(1)



fig = plt.figure(figsize=(11, 11))  # instantiate a figure to draw
ax1 = plt.axes()  # create an axes object

fig.patch.set_facecolor('black') # sets the colour surrounding the Plot to black
fig.patch.set_alpha(0.5) # and makes it more transparent

ax1.get_xaxis().set_visible(False) # hide the y-axis
ax1.get_yaxis().set_visible(False) # hide the x-axis

Slider_ax = plt.axes([0.1, 0.1, 0.8, 0.03]) # set slider coordinates
alpha = Slider(ax=Slider_ax, label=r"$\alpha$", valmin=0, valmax=360, valinit=90)
'''
alpha is the angle of rotation between the two complex planes
it can be changed by clicking on the slider or pressing x/X 
'''

plt.subplots_adjust(bottom = 0.15)

Point = complex(0,0)
div_Point = complex(0,0)
div_Points_plt = ax1.plot([], [], 'C3', zorder=2, lw=2, alpha = 0.25)[0]
div_Points_sct = ax1.scatter([],[], marker='o',color="red", zorder = 3, s = 2)
Frame_Points = []
Mandel = True
Julia = True
Draw_Div_Points = True
# initiate default values that will not be changed by "reset"

reset()
update(1)
# initiate the rest of the default values and draw the plot

fig.canvas.mpl_connect('button_press_event', button_press)
fig.canvas.mpl_connect('button_release_event', button_release)
fig.canvas.mpl_connect('key_press_event', key_press)
fig.canvas.mpl_connect('motion_notify_event', motion_notify_callback)
alpha.on_changed(update)
# connect events to corresponding functions

plt.show()