import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors

Iter = 1
Mandel = True
Power = 2

def Jul(x,y,x_,y_):
    global Iter, Power
    z = np.zeros((len(x),len(x)),dtype=complex)
    z.real = x
    z.imag = y 
    c = np.zeros((len(x),len(x)),dtype=complex)
    c.real += x_
    c.imag += y_
    for i in range(Iter):
        z = z**Power+c
    return(z.real,z.imag)

def Mand(x,y,x_,y_):
    global Iter, Power
    c = np.zeros((len(x),len(x)),dtype=complex)
    c.real = x
    c.imag = y 
    z = np.zeros((len(x),len(x)),dtype=complex)
    z.real += x_
    z.imag += y_
    for i in range(Iter):
        z = z**Power+c
    return(z.real,z.imag)

def Cal(x,y,x_,y_):
    global Mandel
    if Mandel:
        return Mand(x,y,x_,y_)
    else:
        return Jul(x,y,x_,y_)

def dir(x,y,x_,y_):
    x_dir, y_dir = Cal(x,y,x_,y_)
    n = 2*np.sqrt(x_dir**2+y_dir**2)
    return x_dir/n, y_dir/n

def motion_notify_callback(event):
    if event.inaxes != ax1:
        return
    if event.xdata == None or event.ydata == None:
        return
    if event.button == 1:
        x_ = event.xdata
        y_ = event.ydata
        update(x_,y_)

def update(x_,y_):
    x_dir, y_dir = dir(x,y,x_,y_)
    x_s, y_s = Cal(x,y,x_,y_)
    L = np.sqrt(x_s**2+y_s**2)
    Q.set_UVC(x_dir,y_dir,L)
    fig.canvas.draw()

def key_press(event):
    global Iter, Mandel, Power
    if event.key == 'i':
        Iter += 1
    elif event.key == 'I':
        Iter = max(1, Iter-1)
    elif event.key == 'm':
        Mandel = not Mandel
    elif event.key == 'u':
        Power += 1
    elif event.key == 'U':
        Power -= 1

fig = plt.figure(figsize=(11, 11))  # instantiate a figure to draw
ax1 = plt.axes()  # create an axes object

x, y = np.meshgrid(np.linspace(-2,2,128),np.linspace(-2,2,128))

u = x
v = y
L = np.sqrt(u**2+v**2)
Q = ax1.quiver(x,y,u,v,L, cmap='rainbow', units='width', scale = 50)

update(0,0)

fig.canvas.mpl_connect('motion_notify_event', motion_notify_callback)
fig.canvas.mpl_connect('key_press_event', key_press)
plt.show()