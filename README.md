# Mandeljulia

This is a program, with which you can see the plane slices of a 2D complex Space. 
it is made by intersecting the planes of the Mandelbrot and Julia-sets, creating a 4D real or rather 2D complex Space.

# How it works:

Both the Mandelbrot set and the Julia sets are calculated with the equation:
z = z**2 + c
and each pixel is then coloured depending on its divergence when set into the equation.

However the variables have different meanings, 
on the Mandelbrot set z = 0 and c is the coordinate of each pixel,
and on the Julia sets z is the coordinate of each pixel and c is any point on the complex plane.

Therefor we can smoothly change between rendering the Mandelbrot and Julia Sets by rotating the real and imaginary parts of z and c of each equation to one another,

for this we declare alpha to be the angle of rotation on the unitcircle.

the full equation for the rotated plane between the two fractals is still the same, the difference beeing that now:

z = sin(alpha) * coordinate
c = cos(alpha) * coordinate

now adding the offset so that we are not restricted to the Julia set over 0+0i:

c = cos(alpha) * coordinate + sin(alpha) * Offset

similarily we can move the Mandelbrot plane as well:

z = sin(alpha) * coordinate + cos(alpha) * Offset
