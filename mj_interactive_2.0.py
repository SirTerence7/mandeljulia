#Copyright (c) Vispy Development Team. All Rights Reserved.
#
# Building on Mandelbrot in VisPy by John David Reaver
# https://github.com/vispy/vispy/blob/main/examples/demo/gloo/mandelbrot.py
#
from vispy import app, gloo
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from matplotlib import colors
import cv2

vertex = """
attribute vec2 position;
void main()
{
    gl_Position = vec4(position, 0.0, 1.0);
}
"""

fragment = """
uniform vec2 resolution;
uniform vec2 center;
uniform float scale;
uniform float theta;
uniform vec2 off;
uniform int Axis_Choice;
uniform int power;

vec4 hot(float t)
{
    return vec4(smoothstep(0.00,0.66,sqrt(t)),
                smoothstep(0.00,0.5,sqrt(t)),
                smoothstep(0.33,0.75,sqrt(t)),
                1.0);
}

void main()
{
    const int n = 1000;
    const float log_2 = 0.6931471805599453;
    float alpha = radians(theta);
    float co = cos(alpha);
    float si = sin(alpha);

    vec2 c, z;

    if (Axis_Choice == 0) {
        c.x = ((gl_FragCoord.x / resolution.x - 0.5) * scale + center.x) * co + off.x * si;
        c.y = ((gl_FragCoord.y / resolution.y - 0.5) * scale + center.y) * co + off.y * si;

        z.x = ((gl_FragCoord.x / resolution.x - 0.5) * scale + center.x) * si + off.x * co;
        z.y = ((gl_FragCoord.y / resolution.y - 0.5) * scale + center.y) * si + off.y * co;
    }
    else if (Axis_Choice == 1){
        c.x = ((gl_FragCoord.x / resolution.x - 0.5) * scale + center.x) * co + off.x * si;
        z.x = ((gl_FragCoord.y / resolution.y - 0.5) * scale + center.y) * co + off.y * si;

        c.y = ((gl_FragCoord.x / resolution.x - 0.5) * scale + center.x) * si + off.x * co;
        z.y = ((gl_FragCoord.y / resolution.y - 0.5) * scale + center.y) * si + off.y * co;
    }
    else if (Axis_Choice == 2){
        c.x = ((gl_FragCoord.x / resolution.x - 0.5) * scale + center.x) * co + off.x * si;
        z.y = ((gl_FragCoord.y / resolution.y - 0.5) * scale + center.y) * co + off.y * si;

        z.x = ((gl_FragCoord.x / resolution.x - 0.5) * scale + center.x) * si + off.x * co;
        c.y = ((gl_FragCoord.y / resolution.y - 0.5) * scale + center.y) * si + off.y * co;
    }

    float t_x, x, y, d;
    int i, j;

    for(i = 0; i < n; i++) {
        x = z.x;
        y = z.y;
        for(j=1; j<power; j++){
            t_x = (z.x*x - z.y*y);
            y = (z.y*x + z.x*y);
            x = t_x;
        }
        x = x + c.x;
        y = y + c.y;
        d = x*x + y*y;
        if (d > 4.0) break;
        z = vec2(x,y);
    }

    if ( i < n ) {
        float nu = log(log(sqrt(d))/log_2)/log_2;
        float index = float(i) + 3.0 - nu;
        float v = sqrt(index/float(n));
        gl_FragColor = hot(v);
    } else {
        gl_FragColor = hot(0.0);
    }
}
"""

# vispy Canvas
# -----------------------------------------------------------------------------
class Canvas(app.Canvas):

    def __init__(self, *args, **kwargs):
        app.Canvas.__init__(self, *args, **kwargs,  title='Mandeljulia')
        self.program = gloo.Program(vertex, fragment)

        self.program["position"] = [(-1, -1), (-1, 1), (1, 1), (-1, -1), (1, 1), (1, -1)]
        self.reset_values()

        self.bounds = [-3, 3]
        self.min_scale = 0
        self.max_scale = 32

        self.recording = False

        gloo.set_clear_color(color='black')

        self._timer = app.Timer('auto', connect=self.update, start=True)
        self.timer = app.Timer('auto', self.on_timer)
        self.click_Catcher = app.Timer('auto', iterations=30)
        self.last_event_pos = [0, 0]
        self.image_list = []
        self.show()

    def reset_values(self):
        self.scale = self.program["scale"] = 3
        self.center = self.program["center"] = [-0.5, 0]
        self.theta = self.program["theta"] = 0
        self.off = self.program["off"] = [0, 0] # so that triple click is only called when the mouse hasn't moved
        self.Choice = self.program["Axis_Choice"] = 0
        self.power = self.program["power"] = 2
        self.Circle_Center = [0,0]

    def on_draw(self, event):
        self.program.draw()
        if self.recording:
            self.image_list.append((gloo.util._screenshot(alpha = False)))

    def on_resize(self, event):
        self.apply_zoom()

    def on_timer(self, event):
        self.theta = self.program['theta'] = self.theta + self.scale/6

    def apply_zoom(self):
        width, height = self.physical_size
        gloo.set_viewport(0, 0, width, height)
        self.program['resolution'] = [width, height]

    def on_mouse_move(self, event):
        """ Left_Drag:  Move the Plane.
            Middle_Drag: Move the orthogonal Plane absolute.
            Right_Drag: Move the orthogonal Plane.
        """
        if event.is_dragging:
            x0, y0 = event.last_event.pos[0], event.last_event.pos[1]
            x1, y1 = event.pos[0], event.pos[1]
            X0, Y0 = self.pixel_to_coords(float(x0), float(y0))
            X1, Y1 = self.pixel_to_coords(float(x1), float(y1))
            for button in event.buttons:
                if button == 1:
                    self.translate_center(X1 - X0, Y1 - Y0)
                elif button == 2:
                    self.translate_off(X1-X0, Y1-Y0)
                elif button == 3:
                    self.off = self.program["off"] = [X1, Y1]

    def translate_center(self, dx, dy):
        """Translates the center point, and keeps it in bounds."""
        center = self.center
        center[0] -= dx
        center[1] -= dy
        center[0] = min(max(center[0], self.bounds[0]), self.bounds[1])
        center[1] = min(max(center[1], self.bounds[0]), self.bounds[1])
        self.program["center"] = self.center = center

    def translate_off(self, dx, dy):
        """Translates the off point"""
        offset = self.off
        offset[0] -= dx
        offset[1] -= dy
        self.off = self.program["off"] = offset

    def pixel_to_coords(self, x, y):
        """Convert pixel coordinates to Mandelbrot set coordinates."""
        rx, ry = self.size
        nx = (x / rx - 0.5) * self.scale + self.center[0]
        ny = ((ry - y) / ry - 0.5) * self.scale + self.center[1]
        return [nx, ny]
    
    def on_mouse_press(self, event):
        """ Triple Left Click: Sets new Center for Off_Rotation
            Middle Mouse: Moves the orthogonal Plane
            
            Todo (?):
            Triple Right Click: Sets new Center for Animation-Rotation
        """
        x1, y1 = event.pos[0], event.pos[1]
        X1, Y1 = self.pixel_to_coords(float(x1), float(y1))
        if event.button == 1:
            if self.click_Catcher.running:
                x0, y0 = self.last_event_pos[0], self.last_event_pos[1]
                if x0%2 == x1%2 and y0%2 == y1%2:
                    # %2: little movements are tolerated
                    self.click_Catcher.stop()
                    self.Circle_Center = [X1, Y1]
                    print("New Rotation-Center set to: ",self.Circle_Center)
            else:
                self.last_event_pos[0], self.last_event_pos[1] = x1, y1
                self.click_Catcher.start(iterations=30)
        elif event.button == 3:
            self.off = self.program["off"] = [X1, Y1]

    def on_mouse_wheel(self, event):
        """Use the mouse wheel to zoom."""
        delta = event.delta[1]
        if delta > 0:  # Zoom in
            factor = 0.9
        elif delta < 0:  # Zoom out
            factor = 1 / 0.9
        for _ in range(int(abs(delta))):
            self.zoom(factor, event.pos)

    def on_key_press(self, event):
        '''Keyfunctions:
        wasd/WASD: moves the Plane/Orthogonal Plane
        +/*/-/_: zooms in/out (/greatly) of the current figure
        z/Z: rotates the plane
        m/j/M/J: sets alpha to 0/90/180/270 to show mandelbrot or current julia set
        r/R: resets some/all values
        p/P: saves a screenshot (P with hillshading)
        x/y: sets the Off_Point to the x/y axis
        X/Y: Mirrors Offpoint on x/y axis
        i/I: Turn 45 Degrees / Inverts the Off-Point and turns 180 degrees
        u/U: rotate Off_Point around the center
        1...9: n -> z**n
        0/c/C: Chooses Which Axis should be represented cx-cy/cx-zx/cx-zy
        '''
        if event.text == 'w':
            self.translate_center(0, -0.1*self.scale/3)
        elif event.text == 'a':
            self.translate_center(0.1*self.scale/3, 0)
        elif event.text == 's':
            self.translate_center(0, 0.1*self.scale/3)
        elif event.text == 'd':
            self.translate_center(-0.1*self.scale/3, 0)
        elif event.text == 'W':
            self.translate_off(0, -0.1*self.scale/3)
        elif event.text == 'A':
            self.translate_off(0.1*self.scale/3, 0)
        elif event.text == 'S':
            self.translate_off(0, 0.1*self.scale/3)
        elif event.text == 'D':
            self.translate_off(-0.1*self.scale/3, 0)
        elif event.text == '+':
            self.zoom(0.9)
        elif event.text == '-':
            self.zoom(1/0.9)
        elif event.text == '*':
            self.zoom(0.729)
        elif event.text == '_':
            self.zoom(1/0.729)
        elif event.text == 'Z':
            self.program['theta'] = self.theta = self.theta + self.scale/6
        elif event.text == 'z':
            self.program['theta'] = self.theta = self.theta - self.scale/6
        elif event.text == 'm':
            self.program['theta'] = self.theta = 0
        elif event.text == 'j':
            self.program['theta'] = self.theta = 90
        elif event.text == 'M':
            self.program['theta'] = self.theta = 180
        elif event.text == 'J':
            self.program['theta'] = self.theta = 270
        elif event.text == 'r':
            self.program['theta'] = self.theta = 0
            self.program['off'] = self.off = [0,0]
        elif event.text == 'R':
            self.reset_values()
        elif event.text == 'x':
            self.off = self.program["off"] = [self.off[0], 0]
        elif event.text == 'y':
            self.off = self.program["off"] = [0, self.off[1]]
        elif event.text == 'X':
            self.off = self.program["off"] = [self.off[0], -self.off[1]]
        elif event.text == 'Y':
            self.off = self.program["off"] = [-self.off[0], self.off[1]]
        elif event.text == 'i':
            self.program['theta'] = self.theta = self.theta + 45
        elif event.text == 'I':
            self.program['theta'] = self.theta = self.theta + 180
            self.off = self.program["off"] = [-self.off[0], -self.off[1]]
        elif event.text == 'u':
            x = self.off[0]-self.Circle_Center[0]
            y = self.off[1]-self.Circle_Center[1]
            self.rot = np.deg2rad(1*self.scale)
            self.off = self.program["off"] = [np.cos(self.rot)*x-np.sin(self.rot)*y + self.Circle_Center[0], np.sin(self.rot)*x+np.cos(self.rot)*y + self.Circle_Center[1]]
        elif event.text == 'U':
            x = self.off[0]-self.Circle_Center[0]
            y = self.off[1]-self.Circle_Center[1]
            self.rot = np.deg2rad(1*self.scale)
            self.off = self.program["off"] = [np.cos(-self.rot)*x-np.sin(-self.rot)*y + self.Circle_Center[0], np.sin(-self.rot)*x+np.cos(-self.rot)*y + self.Circle_Center[1]]
        elif event.text == 'p' or event.text == 'P':
            image = np.float64(gloo.util._screenshot(alpha = False))/255
            #image = np.float64(canvas.render(alpha=True))/255
            dt_string = datetime.now().strftime("%Y_%m_%d_%H_%M")
            if event.text == 'P':
                light = colors.LightSource(azdeg=315, altdeg=10)
                shade = light.hillshade(image[:,:,0] + image[:,:,1] + image[:,:,2], vert_exag=10.5)/2
                for i in range(3):
                    image[:,:,i] +=  shade
                image = (image-np.min(image))/(np.max(image)-np.min(image))
            plt.imsave("Output_Archive/MandelJulia/MandelJulia_Screenshot_" + dt_string + ".png", image)
            print("Image saved as: ", "MandelJulia_Screenshot_" + dt_string + ".png")
        elif event.text == ' ':
            if self.timer.running:
                self.timer.stop()
            else:
                self.timer.start()
        elif event.text == '1' or event.text == '2' or event.text == '3' or event.text == '4' or event.text == '5' or event.text == '6' or event.text == '7' or event.text == '8' or event.text == '9':
            self.power = self.program["power"] = int(event.text)
        elif event.text == '0':
            self.Choice = self.program["Axis_Choice"] = 0
        elif event.text == 'c':
            self.Choice = self.program["Axis_Choice"] = 1
        elif event.text == 'C':
            self.Choice = self.program["Axis_Choice"] = 2
        elif event.text == 'v':
            self.recording = not self.recording
            if self.recording:
                print("recording...")
            else:
                print("recording stopped, saving video...")
                dt_string = datetime.now().strftime("%Y_%m_%d_%H_%M")
                out = cv2.VideoWriter("Output_Archive/MandelJulia/MandelJulia_Video_" + dt_string + ".mp4",
                                        cv2.VideoWriter_fourcc('m', 'p', '4', 'v'),
                                        60, 
                                        self.image_list[0].shape[0:2], 
                                        isColor = True)
                for image in self.image_list:
                    out.write(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
                out.release()
                self.image_list.clear()
                print("video saved")

    def zoom(self, factor, mouse_coords=None):
        """Factors less than zero zoom in, and greater than zero zoom out.
        If mouse_coords is given, the point under the mouse stays stationary
        while zooming. mouse_coords should come from MouseEvent.pos.
        """
        if mouse_coords is not None:  # Record the position of the mouse
            x, y = float(mouse_coords[0]), float(mouse_coords[1])
            x0, y0 = self.pixel_to_coords(x, y)

        self.scale *= factor
        self.program["scale"] = self.scale = min(self.scale, self.max_scale)

        # Translate so the mouse point is stationary
        if mouse_coords is not None:
            x1, y1 = self.pixel_to_coords(x, y)
            self.translate_center(x1 - x0, y1 - y0)


if __name__ == '__main__':
    canvas = Canvas(size=(1000, 1000), keys='interactive')
    app.run()